package com.example.springmessaging;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class DemoApplicationTests {

    @Autowired
    @Qualifier("sensorDataChannel")
    private SubscribableChannel sensorDataChannel;

    @Test
    public void contextLoads() {
    }

    @Test
    public void isDataFlowing() {

        
        this.sensorDataChannel.subscribe(new MessageHandler() {

            @Override
            public void handleMessage(Message<?> message) throws MessagingException {
                log.info(message.toString());
                Assert.assertNotNull("Test failed", message);

            }
        });
//        Message<?> reply = sensorDataChannel.
//        assertNotNull("reply should not be null", reply)

    }


}

