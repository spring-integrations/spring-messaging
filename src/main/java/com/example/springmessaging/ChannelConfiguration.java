package com.example.springmessaging;

import com.example.springmessaging.boundary.SensorDataBinding;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

@Configuration
@EnableBinding({SensorDataBinding.class, Processor.class})
public class ChannelConfiguration {

    @Bean(name = "arkChannel")
    public SubscribableChannel arkChannel() {
        return new DirectChannel();
    }

    @Bean(name = "sensorDataChannel")
    public SubscribableChannel sensorDataChannel() {
        return new PublishSubscribeChannel();
    }

    @Bean(name = "unMappedChannel")
    public SubscribableChannel unMappedChannel() {
        return new PublishSubscribeChannel();
    }

    @Bean(name = "erreurChannel")
    public SubscribableChannel errorChannel() {
        return new PublishSubscribeChannel();
    }


    @Bean(name = "queryChannel")
    public SubscribableChannel queryChannel() {
        return new DirectChannel();
    }

    @Bean(name = "responseChannel")
    public SubscribableChannel responseChannel() {
        return new DirectChannel();
    }




}