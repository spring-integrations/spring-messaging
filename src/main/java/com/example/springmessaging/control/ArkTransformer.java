package com.example.springmessaging.control;

import com.example.springmessaging.entity.SensorMeasurement;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.Splitter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Component
public class ArkTransformer {

    @Autowired
    private ArkTagMapper tagMapper;

    @Splitter(inputChannel = "arkChannel", outputChannel = "sensorDataChannel")
    public List<SensorMeasurement> transform(JsonNode arkFrame) {
        log.info("Converting ark frame");
        ArrayList<SensorMeasurement> result = new ArrayList<SensorMeasurement>();

        try {
            ArrayNode arrayFrame = (ArrayNode) arkFrame;
            for (JsonNode node : arrayFrame) {
                Date timestamp = this.convertDate(node.get("time").asText());
                node.fields().forEachRemaining(entry -> {
                    SensorMeasurement measurement = new SensorMeasurement();
                    measurement.setTimestamp(timestamp);
                    measurement.setInstallationId("1023-007");
                    measurement.setAssetId("engine-1");
                    if (!entry.getKey().equalsIgnoreCase("time")) {
                        measurement.setTagName(tagMapper.getInternalTag(entry.getKey()));
                        measurement.setValue(entry.getValue().asDouble());
                        result.add(measurement);

                    }
                });

            }
        } catch (Exception ex) {
            log.error("Conversion error", ex);
        }

        return result;

    }

    private Date convertDate(String dateString) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("DD.MM.YY HH:mm:ss");
        return formatter.parse(dateString);
    }

}