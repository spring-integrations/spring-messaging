package com.example.springmessaging.control;

import com.example.springmessaging.entity.SensorMeasurement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SensorDataOutSender {

    @Transformer(inputChannel = "sensorDataChannel", outputChannel = "sensor-data-out")
    public Message<SensorMeasurement> addHeader(SensorMeasurement input) {
        log.info(String.format("Adding header and send out %s", input));
        return MessageBuilder
                .withPayload(input)
                .setHeader(KafkaHeaders.MESSAGE_KEY, getMessageKey(input))
                .build();

    }

    private byte[] getMessageKey(SensorMeasurement measurement) {
        return (measurement.getAssetId() + measurement.getInstallationId()).getBytes();
    }


}