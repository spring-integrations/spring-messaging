package com.example.springmessaging.control;

import com.example.springmessaging.entity.SensorMeasurement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.http.outbound.HttpRequestExecutingMessageHandler;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MessageReceiver {

    // @ServiceActivator(inputChannel="sensorDataChannel")
    // public void receive(List<SensorMeasurement> data) {
    //     log.info("Received "+data.size() + "items");
    // }

    // @ServiceActivator(inputChannel="splittedDataChannel")
    // public void receiveSingle(SensorMeasurement data) {
    //     log.info("Received (splitted) "+data.getTagName());
    // }



//    @ServiceActivator(inputChannel = "erreurChannel")
//    public void receiveError(Message<?> data)
//    {
//        log.info("Received error " + data.getPayload().toString());
//    }


    @ServiceActivator(inputChannel = "responseChannel")
    public void receive(Message<?> data)
    {
        log.info("Received data " + data.getPayload().toString());
    }


//    @ServiceActivator(inputChannel = "sensorDataChannel")
//    public void receive(Message<?> data)
//    {
//        log.info("Received data " + data.getPayload().toString());
//    }


}