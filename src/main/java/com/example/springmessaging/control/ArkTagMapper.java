package com.example.springmessaging.control;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class ArkTagMapper {

    private Map<String, String> tagMap;

    public ArkTagMapper() {

        this.tagMap = loadMapFromConfig();
    }

    private Map<String, String> loadMapFromConfig() {
        log.info("Loading tag map for Ark");
        HashMap<String, String> result = new HashMap<String, String>();
        result.put("noncanonical:temperature@SCA011TE600PV", "SCA011TE600PV");
        result.put("noncanonical:temperature@SCA011TE517PV", "SCA011TE517PV");
        result.put("noncanonical:temperature@SCA011TE527PV", "SCA011TE527PV");
        result.put("noncanonical:temperature@SCA011TE511PV", "SCA011TE511PV");
        result.put("noncanonical:temperature@SCA011TE5021APV", "SCA011TE5021APV");
        result.put("noncanonical:temperature@SCA011TE5031APV", "SCA011TE5031APV");
        result.put("noncanonical:temperature@SCA011TE5041APV", "SCA011TE5041APV");
        result.put("noncanonical:temperature@SCA011TE5051APV", "SCA011TE5051APV");
        result.put("noncanonical:temperature@SCA011TE5061APV", "SCA011TE5061APV");
        result.put("noncanonical:temperature@SCA011TE5011BPV", "SCA011TE5011BPV");
        result.put("noncanonical:temperature@SCA011TE5041BPV", "SCA011TE5041BPV");
        result.put("noncanonical:temperature@SCA011TE5051BPV", "SCA011TE5051BPV");
        result.put("noncanonical:temperature@SCA011TE5081BPV", "SCA011TE5081BPV");
        result.put("noncanonical:ratio@SCA011GT519PV", "SCA011GT519PV");
        result.put("noncanonical:pressure@SCA011PT601PV", "SCA011PT601PV");
        log.info("Loaded " + result.size() + "tags");

        return result;
    }

    public String getInternalTag(String arkTag) {
        //log.info("Getting internal tag for "+arkTag);
        if (!this.tagMap.containsKey(arkTag)) {
            throw new IllegalArgumentException(String.format("%s has no mapping!", arkTag));
        }
        return this.tagMap.get(arkTag);
    }





    /*
    "noncanonical:temperature@SCA011TE511PV": 31.40000,
    "noncanonical:temperature@SCA011TE521PV": 39.10000,
    "noncanonical:temperature@SCA011TE5011APV": 32.90000,
    "noncanonical:temperature@SCA011TE5021APV": 34.36500,
    "noncanonical:temperature@SCA011TE5031APV": 34.00000,
    "noncanonical:temperature@SCA011TE5041APV": 33.00000,
    "noncanonical:temperature@SCA011TE5051APV": 33.00000,
    "noncanonical:temperature@SCA011TE5061APV": 33.63500,
    "noncanonical:temperature@SCA011TE5071APV": 32.80000,
    "noncanonical:temperature@SCA011TE5081APV": 33.90000,
    "noncanonical:temperature@SCA011TE5011BPV": 34.40000,
    "noncanonical:temperature@SCA011TE5021BPV": 34.40000,
    "noncanonical:temperature@SCA011TE5031BPV": 35.40000,
    "noncanonical:temperature@SCA011TE5041BPV": 34.60000,
    "noncanonical:temperature@SCA011TE5051BPV": 34.60000,
    "noncanonical:temperature@SCA011TE5061BPV": 34.60000,
    "noncanonical:temperature@SCA011TE5071BPV": 33.30000,
    "noncanonical:temperature@SCA011TE5081BPV": 34.40000,
    "noncanonical:ratio@SCA011GT519PV": 4.40000,
    "noncanonical:pressure@SCA011PT601PV": 0.00000, */

}
