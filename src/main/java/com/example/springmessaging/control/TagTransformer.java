package com.example.springmessaging.control;

import com.example.springmessaging.entity.SensorMeasurement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.Transformer;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TagTransformer {

    @Autowired
    private ArkTagMapper tagMapper;


    @Transformer(inputChannel = "unMappedChannel", outputChannel = "sensorDataChannel")
    public SensorMeasurement transform(SensorMeasurement input) {
        String newTag = tagMapper.getInternalTag(input.getTagName());
       // log.info(String.format("Mapping tag name %s to %s", input.getTagName(), newTag));

        return new SensorMeasurement(input.getTimestamp(), newTag, input.getInstallationId(), input.getAssetId(), input.getValue());
    }
}
