package com.example.springmessaging.boundary;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;


public interface SensorDataBinding {

    String SENSOR_DATA_OUT = "sensor-data-out";
    String SENSOR_DATA_IN = "sensor-data-in";

    @Output(SENSOR_DATA_OUT)
    MessageChannel sensorDataOut();

    @Input(SENSOR_DATA_IN)
    SubscribableChannel sensorDataIn();
}
