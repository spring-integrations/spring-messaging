package com.example.springmessaging.boundary;

import com.example.springmessaging.entity.SensorMeasurement;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.http.HttpMethod;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.http.outbound.HttpRequestExecutingMessageHandler;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Slf4j
@Component
public class AnotherSource {

//    @InboundChannelAdapter(value = "sensorDataChannel", poller = @Poller(fixedDelay = "3000", errorChannel = "erreurChannel" ))
//    public String generate() {
//        return "YOLO";
//
//    }

//    @StreamListener(SensorDataBinding.SENSOR_DATA_IN)
//    //@SendTo("sensorDataChannel")
//    public void receive(Message<?> msg){
//        log.warn("RECEIVED FROM KAKFKA {}",msg);
//
//    }

    @ServiceActivator(inputChannel = "queryChannel", outputChannel = "responseChannel")
    public MessageHandler httpOut() {
        HttpRequestExecutingMessageHandler handler = new HttpRequestExecutingMessageHandler("https://jsonplaceholder.typicode.com/todos");
        handler.setHttpMethod(HttpMethod.GET);
        handler.setExpectedResponseType(String.class);
        return handler;


    }

    @InboundChannelAdapter(value = "queryChannel", poller = @Poller(fixedDelay = "3000", errorChannel = "erreurChannel" ))
    public String getData() {
        return "";

    }
}
