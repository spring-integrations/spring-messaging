package com.example.springmessaging.boundary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;


@Slf4j
@Component
public class ArkDataSource {

    private static final String FILENAME = "ark.json";



    @InboundChannelAdapter(value = "arkChannel", poller = @Poller(fixedDelay = "10000", errorChannel = "erreurChannel" ))
    public JsonNode getData() {
        log.info("Loading data");
        ClassLoader classLoader = getClass().getClassLoader();
        File arkData = new File(classLoader.getResource(FILENAME).getFile());
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readTree(arkData);
        } catch (IOException e) {
            log.error("File error");
        }

        return null;
    }

}