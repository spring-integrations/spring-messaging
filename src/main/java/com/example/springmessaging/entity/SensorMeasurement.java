package com.example.springmessaging.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SensorMeasurement implements Serializable {


    /**
     * indicates when the value was measured
     */
    private Date timestamp;

    /**
     * the identification of the sensor (tagname)
     */
    private String tagName;

    /**
     * Installation id
     */
    private String installationId;

    /**
     * Asset id
     */
    private String assetId;

    private double value;

}


